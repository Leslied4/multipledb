class DvlaCitizensController < ApplicationController
  before_action :authenticate_user!
  def index
    @dvla_citizens = DvlaCitizen.all
  end

  def show
    @dvla_citizen = DvlaCitizen.find(params[:id])
  end

  def new
    @dvla_citizen = DvlaCitizen.new
    # @awards = Award.all
  end

  def create
    filename = "#{Time.now.to_i}#{File.extname(dvla_citizen_params[:image].tempfile)}"
    @dvla_citizen = DvlaCitizen.new(dvla_citizen_params.except(:image))
    @dvla_citizen.image = filename

    if @dvla_citizen.save
      flash[:notice] = 'DVLA Record Created!'
      file = dvla_citizen_params[:image].tempfile
      destination = "#{Rails.root}/app/assets/images/#{@dvla_citizen.image}"
      File.copy_stream(file, destination)
      redirect_to @dvla_citizen
    else
      render :new
    end
  end

  private

  def dvla_citizen_params
    params.require(:dvla_citizen).permit(:first_name, :last_name, :general_id, :description, :image)
  end
end
