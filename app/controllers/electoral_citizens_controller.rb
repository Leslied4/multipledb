class ElectoralCitizensController < ApplicationController
  before_action :authenticate_user!
  def index
    @electoral_citizens = ElectoralCitizen.all
  end

  def show
    @electoral_citizen = ElectoralCitizen.find(params[:id])
  end

  def new
    @electoral_citizen = ElectoralCitizen.new
    # @awards = Award.all
  end

  def create
    filename = "#{Time.now.to_i}#{File.extname(electoral_citizen_params[:image].tempfile)}"
    @electoral_citizen = ElectoralCitizen.new(electoral_citizen_params.except(:image))
    @electoral_citizen.image = filename

    if @electoral_citizen.save
      flash[:notice] = 'Electoral Commission Record Created!'
      file = electoral_citizen_params[:image].tempfile
      destination = "#{Rails.root}/app/assets/images/#{@electoral_citizen.image}"
      File.copy_stream(file, destination)
      redirect_to @electoral_citizen
    else
      render :new
    end
  end

  private

  def electoral_citizen_params
    params.require(:electoral_citizen).permit(:first_name, :last_name, :general_id, :description, :image)
  end
end
