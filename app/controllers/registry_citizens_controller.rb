class RegistryCitizensController < ApplicationController
  before_action :authenticate_user!
  def index
    @registry_citizens =RegistryCitizen.all
  end

  def show
    @registry_citizen = RegistryCitizen.find(params[:id])
  end

  def new
    @registry_citizen = RegistryCitizen.new
    # @awards = Award.all
  end

  def create
    filename = "#{Time.now.to_i}#{File.extname(registry_citizen_params[:image].tempfile)}"
    @registry_citizen = RegistryCitizen.new(registry_citizen_params.except(:image))
    @registry_citizen.image = filename

    if @registry_citizen.save
      flash[:notice] = 'Birth And Death Registry Record Created!'
      file = registry_citizen_params[:image].tempfile
      destination = "#{Rails.root}/app/assets/images/#{@registry_citizen.image}"
      File.copy_stream(file, destination)
      redirect_to @registry_citizen
    else
      render :new
    end
  end

  private
  def registry_citizen_params
    params.require(:registry_citizen).permit(:first_name, :last_name, :general_id, :description, :image)
  end
end
