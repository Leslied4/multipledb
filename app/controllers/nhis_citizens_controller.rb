class NhisCitizensController < ApplicationController
  before_action :authenticate_user!
  def index
    @nhis_citizens = NhisCitizen.all
  end

  def show
    @nhis_citizen = NhisCitizen.find(params[:id])
  end

  def new
    @nhis_citizen = NhisCitizen.new
    # @awards = Award.all
  end

  def create
    filename = "#{Time.now.to_i}#{File.extname(nhis_citizen_params[:image].tempfile)}"
    @nhis_citizen = NhisCitizen.new(nhis_citizen_params.except(:image))
    @nhis_citizen.image = filename

    if @nhis_citizen.save
      flash[:notice] = 'NHIS Record Created!'
      file = nhis_citizen_params[:image].tempfile
      destination = "#{Rails.root}/app/assets/images/#{@nhis_citizen.image}"
      File.copy_stream(file, destination)
      redirect_to @nhis_citizen
    else
      render :new
    end
  end

  private

  def nhis_citizen_params
    params.require(:nhis_citizen).permit(:first_name, :last_name, :general_id, :description, :image)
  end
end
