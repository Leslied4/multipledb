class AllCitizensController < ApplicationController
  before_action :authenticate_user!

  def index
    @registry_citizens = RegistryCitizen.all
    @electoral_citizens = ElectoralCitizen.all
    @nhis_citizens = NhisCitizen.all
    @dvla_citizens = DvlaCitizen.all
  end

  def create
    key = all_citizen_params["key"].strip
    search = all_citizen_params["gender"]

    ActiveRecord::Base.connected_to(role: :reading) do
      # all code in this block will be connected to the reading role
    end

    if search == "1"
      @result = [RegistryCitizen, ElectoralCitizen, NhisCitizen, DvlaCitizen].map do |model|
        model.where('last_name LIKE ?', key)
      end
    elsif search == "2"
      @result = [RegistryCitizen, ElectoralCitizen, NhisCitizen, DvlaCitizen].map do |model|
        model.where('general_id LIKE ?', key.to_i)
      end
    else
      @result = [RegistryCitizen, ElectoralCitizen, NhisCitizen, DvlaCitizen].map do |model|
        model.where('first_name LIKE ?', key)
      end
    end

    session[:result] = @result

    # @registry_citizens = []
    # @electoral_citizens = []
    # @nhis_citizens = []
    # @dvla_citizens = []

    redirect_to all_citizens_info_path
  end

  def info; end

  def show; end

  private

  def all_citizen_params
    params.permit(:key, :gender, :authenticity_token)
  end
end
