# award

Just a dummy
Install a version of ruby preferrably below 2.7
Update Gemfile and the .ruby-version file
to the version of ruby downloaded, 
ruby '2.6.8'

Run the following commands in the project directory

Next run
  *gem install bundler:2.1.4*

Next run
  *bundle install*
To install all the libraries used in the project

Make sure you have yarn installed on your system
Next run
  *yarn install --check-files*
To install the packages used for the project

make sure to update the application.yml with the credentials for your databases

Next run
  *rails db:create*
To create the databases

Next run
  *rails db:migrate*
To run the SQL database migrations 

To start the server run
  *rails server*

To start a console application run
  *rails console*